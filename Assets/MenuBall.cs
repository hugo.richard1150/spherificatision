﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBall : MonoBehaviour
{
    public bool isBall;

    // Start is called before the first frame update
    void Start()
    {
        if (isBall)
        {
            StartCoroutine(WaitAndJump());
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, 0.1f));    
    }

    public IEnumerator WaitAndJump()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * 300);
        yield return new WaitForSeconds(3f);
        StartCoroutine(WaitAndJump());
    }
}
