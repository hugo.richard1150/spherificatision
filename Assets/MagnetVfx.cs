﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetVfx : MonoBehaviour
{
    private GameObject player;

    public GameObject vfx;

    public bool isActive = false;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>().gameObject;
        vfx.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        /*if (isActive)
        {
            vfx.GetComponentInChildren<ParticleSystem>().enableEmission = true;
            vfx.transform.LookAt(player.transform.position);
            isActive = false;
        }
        else
        {
            vfx.GetComponentInChildren<ParticleSystem>().enableEmission = false;
        }*/

        if (isActive)
        {
            if (player.GetComponent<Magnet>().useMagnet)
            {
                //vfx.GetComponentInChildren<ParticleSystem>().enableEmission = true;
                vfx.SetActive(true);
                vfx.transform.LookAt(player.transform.position);
                float size = Vector3.Distance(player.transform.position, transform.position);
                vfx.transform.localScale = new Vector3(1, 1, size/5 + 0.1f);
                Debug.Log(size);
            }
            else
            {
                //vfx.GetComponentInChildren<ParticleSystem>().enableEmission = false;
                vfx.SetActive(false);
            }
        }
        else
        {
            //vfx.GetComponentInChildren<ParticleSystem>().enableEmission = false;
            vfx.SetActive(false);
        }
        
        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player" && gameObject.GetComponent<CubeToSphere>().isSphere/*&& player.GetComponent<Magnet>().useMagnet*/)
        {
            isActive = true;
        }
        else
        {
            isActive = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player" && gameObject.GetComponent<CubeToSphere>().isSphere/*&& player.GetComponent<Magnet>().useMagnet*/)
        {
            isActive = false;
        }
    }
}
