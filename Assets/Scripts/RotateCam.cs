﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCam : MonoBehaviour
{
    public float SpeedRotate = 1f;

    void Update()
    {
        transform.Rotate(Vector3.up * SpeedRotate * Time.deltaTime);
    }
}
