﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DoorDisappear : MonoBehaviour
{
    public int[] points;
    public int doorID;

    private AudioSource audioSource;
    public AudioClip completeObjectifSound;

    

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(points[0] == 10 && doorID == 0)
        {
            points[0]++;
            audioSource.clip = completeObjectifSound;
            audioSource.volume = 1f;
            audioSource.pitch = 0.5f;
            audioSource.Play();
            //GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
            //GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
            gameObject.transform.Find("Porte0").gameObject.SetActive(false);
        }

        if(points[1] == 4 && doorID == 1)
        {
            points[1]++;
            //GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
            //GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
            gameObject.transform.Find("Porte1").gameObject.SetActive(false);
            audioSource.clip = completeObjectifSound;
            audioSource.volume = 1f;
            audioSource.pitch = 0.5f;
            audioSource.Play();
        }

        if (points[2] == 5 && doorID == 2)
        {
            points[2]++;
            /*GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
            GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
            GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
            GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);*/
            gameObject.transform.Find("Porte2").gameObject.SetActive(false);
            gameObject.transform.Find("Porte3").gameObject.SetActive(false);
            audioSource.clip = completeObjectifSound;
            audioSource.volume = 1f;
            audioSource.pitch = 0.5f;
            audioSource.Play();
        }
    }
}
