﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySphere : MonoBehaviour
{
    public GameObject cube;
    public Vector3[] directions;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Balls")
        {
            if (other.GetComponent<CubeToSphere>().isSphere)
            {
                Destroy(other.gameObject);
                GameObject newCube = Instantiate(cube);
                newCube.transform.position = transform.position;
                //newCube.GetComponentInChildren<Rigidbody>().AddForce(new Vector3(Random.Range(-1f, 1f), 1, Random.Range(-1f, 1f)).normalized * 1000);
                newCube.GetComponentInChildren<Rigidbody>().velocity = Vector3.zero;
                newCube.GetComponentInChildren<Rigidbody>().AddForce(directions[Random.Range(0, directions.Length)] * 50);
            }
        }      
    }
}
