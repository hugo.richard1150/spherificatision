﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Variables déplacement XYZ:")]
    public float speed;
    public float speedJump;

    private Rigidbody rb;

    [Header("Détéction du sol:")]
    public bool isGrounded;
    private RaycastHit hit;
    public LayerMask layerGround;

    private GameObject cam;
    private Vector3 dir;

    [Header("Variables respawn:")]
    public Vector3 respawnPoints;
    public float Ymax;

    [Header("Vfx:")]
    public bool doOnce = false;
    public GameObject jumpVfx;
    public GameObject slamVfx;
    public bool doOnceSpeed = false;
    public GameObject speedVfx;
    public float speedToVfx;

    [Header("SD:")]
    private AudioSource audioSource;
    public AudioClip jumpSound;
    private bool playFallSoundOnce;
    public AudioClip fallSound;
    public AudioClip gravityEnter;
    public AudioClip gravityExit;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = GameObject.FindGameObjectWithTag("MainCamera");
        audioSource = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        //Déplacement
        if (Input.GetAxis("Vertical") > 0)
        {
            dir = (transform.position - cam.transform.position).normalized;
            rb.velocity = Vector3.Lerp(rb.velocity, dir * speed * Time.deltaTime, Time.deltaTime);
            rb.AddForce(Vector3.Lerp(rb.velocity, dir * speed * Time.deltaTime, Time.deltaTime), ForceMode.Force);
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            rb.velocity = Vector3.Lerp(rb.velocity, Vector3.zero, Time.deltaTime * 5);
        }
        else
        {
            rb.velocity = Vector3.Lerp(rb.velocity, Vector3.zero, Time.deltaTime);
        }

        //Saut
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            audioSource.clip = jumpSound;
            audioSource.volume = 0.1f;
            audioSource.Play();

            //rb.velocity = rb.velocity + Vector3.up * speedJump * Time.deltaTime;
            rb.AddForce(Vector3.up * speedJump, ForceMode.Force);
            
            if (Physics.Raycast(transform.position, -Vector3.up, out hit, Mathf.Infinity))
            {
                GameObject newJump = Instantiate(jumpVfx);
                newJump.transform.position = hit.point;
                Destroy(newJump, 3f);
            }
        }

        if(isGrounded && doOnce == false)
        {
            if (Physics.Raycast(transform.position, -Vector3.up, out hit, Mathf.Infinity))
            {
                doOnce = true;
                GameObject newSlam = Instantiate(slamVfx);
                newSlam.transform.position = hit.point;
                Destroy(newSlam, 3f);
            }
        }

        //Raycast de détéctio si le joueur est au sol
        if(Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity, layerGround))
        {
            Debug.DrawRay(transform.position, Vector3.down * hit.distance, Color.green);

            if (hit.distance < 1f)
            {
                isGrounded = true;

                if (!playFallSoundOnce)
                {
                    audioSource.clip = fallSound;
                    audioSource.volume = 0.2f;
                    audioSource.Play();

                    playFallSoundOnce = true;
                }
            }
            else
            {
                isGrounded = false;
                doOnce = false;
                playFallSoundOnce = false;
            }
        }
        else
        {
            isGrounded = false;
            doOnce = false;
        }
    }

    private void Update()
    {
        if (transform.position.y <= Ymax)
        {
            Respawn();
        }

        if(rb.velocity.magnitude >= speedToVfx)
        {
            speedVfx.SetActive(true);
            speedVfx.gameObject.GetComponentInChildren<ParticleSystem>().enableEmission = true;
        }
        else
        {
            speedVfx.gameObject.GetComponentInChildren<ParticleSystem>().enableEmission = false;
        }
        speedVfx.transform.position = transform.position;
        speedVfx.transform.forward = -rb.velocity.normalized;

    }

    public void Respawn()
    {
        rb.velocity = Vector3.zero;
        transform.position = respawnPoints;
        cam.transform.position = transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ReverseGravity>())
        {
            audioSource.clip = gravityEnter;
            audioSource.volume = 0.1f;
            audioSource.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<ReverseGravity>())
        {
            audioSource.clip = gravityExit;
            audioSource.volume = 0.1f;
            audioSource.Play();
        }
    }
}
