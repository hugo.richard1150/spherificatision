﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Puzzle : MonoBehaviour
{
    public int doorConnected;

    public float timeDestroy;

    public int game;

    public TextMeshProUGUI[] textGoal;
    public float[] goals;

    private GameObject cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        foreach(TextMeshProUGUI texts in textGoal)
        {
            texts.gameObject.transform.parent.LookAt(cam.transform);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Balls" && game == 0)
        {
            Destroy(other.gameObject, timeDestroy);
            GetComponentInParent<DoorDisappear>().points[doorConnected]++;
            textGoal[doorConnected].text = GetComponentInParent<DoorDisappear>().points[doorConnected].ToString() + " / " + goals[doorConnected];
        }

        if (other.gameObject.tag == "Balls" && game == 1)
        {
            GetComponentInParent<DoorDisappear>().points[doorConnected]++;
            textGoal[doorConnected].text = GetComponentInParent<DoorDisappear>().points[doorConnected].ToString() + " / " + goals[doorConnected];
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Balls" && game == 1)
        {
            GetComponentInParent<DoorDisappear>().points[doorConnected]--;
            textGoal[doorConnected].text = GetComponentInParent<DoorDisappear>().points[doorConnected].ToString() + " / " + goals[doorConnected];
        }
    }
}
