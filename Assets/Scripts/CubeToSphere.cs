﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeToSphere : MonoBehaviour
{
    private Animator anim;
    public GameObject Vfx;

    public bool isSphere = false;

    private AudioSource audioSource;
    private bool playTransformationOnce;
    public AudioClip transformationSound;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        anim.enabled = false;
        Vfx.SetActive(false);
    }

    private void Update()
    {
        if (!playTransformationOnce && isSphere)
        {
            audioSource.clip = transformationSound;
            audioSource.volume = 0.5f;
            audioSource.Play();

            playTransformationOnce = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            anim.enabled = true;
            GetComponent<BoxCollider>().enabled = false;
            GetComponent<SphereCollider>().enabled = true;
            Vfx.SetActive(true);
            Vfx.transform.LookAt(collision.transform.position);
            isSphere = true;

            Vector3 directionForce = new Vector3(transform.position.x - collision.gameObject.transform.position.x,
                                                    transform.position.y - collision.gameObject.transform.position.y,
                                                    transform.position.z - collision.gameObject.transform.position.z).normalized;
            collision.gameObject.GetComponent<Rigidbody>().AddForce(directionForce * 10);
        }
    }
}
