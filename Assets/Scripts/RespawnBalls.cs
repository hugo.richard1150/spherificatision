﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnBalls : MonoBehaviour
{
    public float Ymax = -30;

    private Vector3 iniPos;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        iniPos = transform.position;
    }

    private void Update()
    {
        if (transform.position.y <= Ymax)
        {
            rb.velocity = Vector3.zero;
            transform.position = iniPos;
            rb.velocity = Vector3.zero;
        }
    }
}
