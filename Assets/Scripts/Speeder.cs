﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speeder : MonoBehaviour
{
    private GameObject cam;
    private Vector3 dir;

    private GameObject player;

    private Animator anim;

    public float powerPlayer;
    public float powerBall;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        anim.enabled = false;

        cam = GameObject.FindGameObjectWithTag("MainCamera");
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            anim.enabled = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("touch");
            dir = (player.transform.position - cam.transform.position).normalized;
            other.gameObject.GetComponent<Rigidbody>().AddForce(powerPlayer * dir * Time.deltaTime, ForceMode.Force);
        }

        if (other.gameObject.tag == "Balls")
        {
            Debug.Log("touch");
            dir = (player.transform.position - cam.transform.position).normalized;
            other.gameObject.GetComponent<Rigidbody>().AddForce(powerBall * dir * Time.deltaTime, ForceMode.Force);
        }
    }
}
