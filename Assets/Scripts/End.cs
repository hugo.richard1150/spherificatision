﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class End : MonoBehaviour
{
    public CanvasGroup canvasGroup;

    private bool end;
    private float t;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && other.isTrigger == false)
        {
            end = true;
        }
    }

    private void Update()
    {
        if (end)
        {
            t += Time.deltaTime;

            canvasGroup.alpha = t;

            if (canvasGroup.alpha >= 1)
            {
                if (t >= 5)
                {
                    SceneManager.LoadScene(0);
                }
            }
        }
    }
}
