﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractRepulse : MonoBehaviour
{
    public float radiusAttract;
    public Vector3 boxAttract;
    public float power;

    void Start()
    {
        if(GetComponent<SphereCollider>() != null)
        {
            GetComponent<SphereCollider>().radius = radiusAttract;
        }
        if (GetComponent<BoxCollider>() != null)
        {
            GetComponent<BoxCollider>().size = boxAttract;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Balls" && other.GetComponent<CubeToSphere>().isSphere /*|| other.gameObject.tag == "Player"*/)
        {
            other.gameObject.GetComponent<Rigidbody>().useGravity = false;
            Vector3 directionForce = new Vector3(   gameObject.transform.position.x - other.gameObject.transform.position.x,
                                                    gameObject.transform.position.y - other.gameObject.transform.position.y,
                                                    gameObject.transform.position.z - other.gameObject.transform.position.z).normalized;
            float dist = Vector3.Distance(other.transform.position, gameObject.transform.position);
            float distPower = 1.2f - Mathf.Abs(dist / radiusAttract);
            //Debug.Log(distPower);
            other.gameObject.GetComponent<Rigidbody>().AddForce(directionForce * distPower * power);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Balls" /*|| other.gameObject.tag == "Player"*/)
        {
            other.gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
    }
}
