﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour
{
    //Le joueur
    private GameObject player;
    //La position target par la camera
    [SerializeField]
    private Transform targetPosition;
    [SerializeField]
    private float lerpingPositionSpeed = 3.5f;

    private Vector3 lookAtPosition;

    //Axe de la sourie
    private float mouseX;
    private float mouseY;

    //deltaTime
    private float dt;

    //Vitesse de rotation de la camera
    [SerializeField]
    private float rotationSpeedX = 100;
    [SerializeField]
    private float rotationSpeedY = 50;


    //Distance entre la camera et le joueur
    [SerializeField]
    private float maxDistance = 10;
    [SerializeField]
    private float minDistance = 1;
    private float distance;

    //Différence entre la position du joueur et la position initiale de la camera
    private Vector3 offsetPos;
    

    //Ancien
    [Header("Ancienne Variables:")]
    private Vector3 pos;
    public float height;

    private bool cameraCollisionGround;

    private RaycastHit _hit;

    [Header("Détéction sol:")]
    public LayerMask layerGround;

    private void Start()
    {
        //Retire la vision du curseur
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        //assignation de la variable player
        player = transform.parent.gameObject;

        //Met la cible de déplacement sur la position actuelle de la camera
        targetPosition.position = transform.position;
        targetPosition.parent.parent = null;

        //Assignation des variables pour tester les collisions
        distance = Vector3.Distance(transform.position, transform.parent.position);
        maxDistance = Vector3.Distance(transform.position, transform.parent.position);

        //Retire la camera du joueur
        transform.parent = null;
        //Différence entre la position de la camera de base et la position du joueur
        offsetPos = transform.position - player.transform.position;
    }

    private void Update()
    {
        //DetectGroundAndAdapt();
    }

    private void FixedUpdate()
    {
        //UpdateCameraPosition();
        dt = Time.fixedDeltaTime;


        RotateCamera();
        TestColliding(targetPosition.parent, targetPosition.transform);
        MakePosition();
        FocusPoint();
    }

    void MakePosition()
    {
        //update la position du parent de targetPosition
        targetPosition.parent.position = player.transform.position;

        //Set la position de la camera
        transform.position = Vector3.Lerp(transform.position, targetPosition.position, lerpingPositionSpeed * dt);
    }

    void FocusPoint()
    {
        transform.LookAt(player.transform.position);
    }

    void RotateCamera()
    {
        //Reçoit les information de mouvement selon l'axe X de la sourie
        mouseX = Input.GetAxis("Mouse X") * rotationSpeedX * dt;
        mouseY = Input.GetAxis("Mouse Y") * rotationSpeedY * dt;

        //Transmet la rotation
        targetPosition.parent.Rotate(0, mouseX, 0);
    }

    void TestColliding(Transform origin, Transform goToAdapt)
    {
        //Direction à partir du parent
        Vector3 directionFromTheParent = offsetPos;
        directionFromTheParent = directionFromTheParent.normalized;

        //Position désiré
        Vector3 desiredPos = Vector3.zero;
        desiredPos = origin.TransformPoint(directionFromTheParent * maxDistance);

        CalculateDistance(origin.position);

        //Raycast depuis le parent vers l'objet
        RaycastHit hit;
        //Si le raycast collide un élément
        void CalculateDistance(Vector3 origin2)
        {
            if (Physics.Linecast(origin2, desiredPos, out hit))
            {
                if (hit.transform != player.transform)
                {
                    //La distance s'update et se recalcule pour placer cet objet devant l'objet qui cache la vision
                    distance = Mathf.Clamp((hit.distance * 0.87f), minDistance, maxDistance);
                }
                else
                {
                    //La distance est la distance maximale
                    distance = maxDistance;
                }
            }
            else
            {
                //La distance est la distance maximale
                distance = maxDistance;
            }
        }

        //Mise à jour de la position locale de l'objet
        goToAdapt.localPosition = directionFromTheParent * distance;

        Debug.DrawRay(player.transform.position, directionFromTheParent * distance);
    }

    #region Ancien Code
    void DetectGroundAndAdapt()
    {
        //Raycast de détéction du sol pour régler la caméra
        if (Physics.Raycast(transform.position, -Vector3.up, out _hit, Mathf.Infinity, layerGround))
        {
            Debug.DrawRay(transform.position, -Vector3.up * _hit.distance, Color.yellow);
            cameraCollisionGround = true;
        }
        else
        {
            cameraCollisionGround = false;
        }
    }

    void UpdateCameraPosition()
    {
        /*        playerDir = target.position - transform.position;
        if (Physics.Linecast(target.position, -playerDir, out hitCollision))
        {
            distance = Vector3.Distance(target.position, hitCollision.point);
        }
        else
        {
            distance = Vector3.Distance(target.position, offssetPos);
        }*/
        pos = player.transform.position + offsetPos;

        if (cameraCollisionGround)
        {
            transform.localPosition = new Vector3(pos.x, Mathf.Lerp(transform.position.y, _hit.point.y + height, Time.deltaTime * 2), pos.z);
        }
        else
        {
            transform.localPosition = new Vector3(pos.x, transform.position.y, pos.z);
        }

        transform.LookAt(player.transform);
        Quaternion camTurnAngleX = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationSpeedX * dt, Vector3.up);
        offsetPos = camTurnAngleX * offsetPos;
    }
    #endregion
}
