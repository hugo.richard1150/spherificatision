﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTps : MonoBehaviour
{
    public Transform playerCameraParent;

    private float distance = 4.0f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;

    public float minY;

    public Transform camTransform;



    public bool cameraCollisionGround;

    private RaycastHit hit;
    private RaycastHit hitCollision;

    public LayerMask layerGround;

    private Vector3 offssetPos;
    private Vector3 pos;

    public float height;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        currentX += Input.GetAxis("Mouse X");
        currentY += -Input.GetAxis("Mouse Y");

        Vector3 dir = new Vector3(0, 0, -distance);
        float Yclamped = Mathf.Clamp(currentY, minY, -minY);
        Quaternion rotation = Quaternion.Euler(Yclamped, currentX, 0);
        camTransform.position = playerCameraParent.position + rotation * dir;
        camTransform.LookAt(playerCameraParent.position);


        if (Physics.Raycast(transform.position, -Vector3.up, out hit, Mathf.Infinity, layerGround))
        {
            Debug.DrawRay(transform.position, -Vector3.up * hit.distance, Color.yellow);
            cameraCollisionGround = true;
        }
        else
        {
            cameraCollisionGround = false;
        }
    }

    private void FixedUpdate()
    {
        if (cameraCollisionGround)
        {
            /*currentX += Input.GetAxis("Mouse X");
            currentY += -Input.GetAxis("Mouse Y");

            Vector3 dir = new Vector3(0, 0, -distance);
            float Yclamped = Mathf.Clamp(currentY, minY, -minY);
            Quaternion rotation = Quaternion.Euler(Yclamped, currentX, 0);
            camTransform.position = playerCameraParent.position + rotation * dir ;*/

            //transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, hit.point.y + height, Time.deltaTime * 2), transform.localPosition.z);
        }
        else
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
        }
    }
}
