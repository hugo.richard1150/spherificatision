﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReverseGravity : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Rigidbody rbOther = other.gameObject.GetComponent<Rigidbody>();
            rbOther.AddForce(Vector3.up* 1000 * Time.deltaTime, ForceMode.Acceleration);
        }
    }
}
