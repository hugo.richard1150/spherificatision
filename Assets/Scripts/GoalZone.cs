﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GoalZone : MonoBehaviour
{
    private int currentGoal;
    public int goalMax;

    public TextMeshProUGUI textGoal;

    private GameObject cam;

    public bool complete;
    public GameObject door;

    private AudioSource audioSource;
    public AudioClip sphereInSound;
    private bool playCompleteOnce = false;
    public AudioClip completeObjectifSound;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        cam = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Update()
    {
        textGoal.text = currentGoal + " / " + goalMax.ToString();
        textGoal.transform.parent.LookAt(cam.transform.position);

        if (currentGoal >= goalMax)
        {
            complete = true;
            door.SetActive(false);

            if (!playCompleteOnce)
            {
                audioSource.clip = completeObjectifSound;
                audioSource.volume = 1f;
                audioSource.pitch = 0.5f;
                audioSource.Play();

                playCompleteOnce = true;
            }
        }
        else
        {
            complete = false;
            door.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<CubeToSphere>())
        {
            currentGoal++;

            audioSource.clip = sphereInSound;
            audioSource.volume = 0.5f;
            audioSource.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<CubeToSphere>())
        {
            currentGoal--;
        }
    }
}
