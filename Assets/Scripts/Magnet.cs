﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Magnet : MonoBehaviour
{
    public float magnetForce;

    public bool useMagnet = false;

    public Image magnetUi;

    private void Start()
    {
        magnetUi.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (useMagnet)
            {
                useMagnet = false;
                magnetUi.gameObject.SetActive(false);
            }
            else
            {
                useMagnet = true;
                magnetUi.gameObject.SetActive(true);
            }
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (useMagnet)
        {
            if (other.gameObject.tag == "Balls" && other.GetComponent<CubeToSphere>().isSphere)
            {
                //other.GetComponentInChildren<ParticleSystem>().gameObject.SetActive(true);
                //other.GetComponentInChildren<ParticleSystem>().gameObject.transform.LookAt(transform.position);
                Vector3 dir = (transform.position - other.gameObject.transform.position).normalized * 0.2f;
                other.gameObject.GetComponent<Rigidbody>().AddForce(dir * magnetForce * Time.deltaTime, ForceMode.Acceleration);
            }
        }
        else
        {
            //other.GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Balls" && other.GetComponent<CubeToSphere>().isSphere)
        {
            //other.GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
        }
    }
}
