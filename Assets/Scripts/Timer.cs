﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float maxTime;
    private float timer;
    private float seconds;

    public Text textTimer;
    public GameObject panelDefeat;

    private void Start()
    {
        panelDefeat.SetActive(false);
        timer = maxTime;
    }

    private void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
            seconds = (int)(timer % 60);
        }
        else
        {
            panelDefeat.SetActive(true);
        }

        textTimer.text = seconds.ToString();
    }

    public void Retry()
    {
        timer = maxTime;
        panelDefeat.SetActive(false);
    }
}
