﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour
{
    private Animator anim;
    public Animator anim2;

    public bool isSquare = true;

    public float powerPlayer;
    public float powerBall;

    private AudioSource audioSource;
    public AudioClip bumpSound;

    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        anim.enabled = false;
        //anim2 = GetComponentInChildren<Animator>();
        anim2.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && isSquare)
        {
            isSquare = false;
            anim.enabled = true;
        }

        if (other.gameObject.tag == "Player" && other.isTrigger == false)
        {
            Debug.Log("touch");
            anim2.enabled = false;
            anim2.enabled = true;
            other.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * powerPlayer);

            audioSource.clip = bumpSound;
            audioSource.volume = 0.3f;
            audioSource.Play();
        }

        if (other.gameObject.tag == "Balls")
        {
            Debug.Log("touch");
            anim2.enabled = false;
            anim2.enabled = true;
            other.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * powerBall);

            audioSource.clip = bumpSound;
            audioSource.volume = 0.3f;
            audioSource.Play();
        }
    }
}
